import { response } from 'express';
import { PrimaryKeyError } from '../common/error/repository-error.model';
import { IUser, UserModel } from './user.model';
import { sequelize } from '../common/database.config';
import { ResolveOptions } from 'node:dns';

class UserRepository <IUser> {
  protected modelClass = UserModel;

  findByEmailAndPassword(pseudo: string, password: string): Promise<any> {
    return UserModel.findOne({
      where: {
        pseudo,
        password
      },
      rejectOnEmpty: true
    });
  }

  findAll(): Promise<IUser> {
    return this.modelClass.findAll() as unknown as Promise<IUser>;
  }

  get(id: number): Promise<IUser> {
    return this.modelClass.findByPk(id, { rejectOnEmpty: true }) as unknown as Promise<IUser>;
  }

  create(model: IUser): Promise<IUser> {
    return this.modelClass.create(model) as unknown as Promise<IUser>;
  }

  update(id: number, model: IUser): Promise<IUser> {
    return this.modelClass.update(model, { where: { id } })
      .then(([affectedRowsCount]) => {
        if (affectedRowsCount) {
          return model;
        } else {
          throw new PrimaryKeyError();
        }
      });
  }

  remove(id: number): Promise<void> {
    return this.modelClass.destroy({ where: { id } })
      .then((affectedRowsCount) => {
        if (!affectedRowsCount) {
          throw new PrimaryKeyError();
        }
      });
  }
}

export const userRepository = new UserRepository();
