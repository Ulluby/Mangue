import { IAuthMeDto } from '../auth/auth.model';
import { ErrorType } from '../common/error/error.model';
import { generateToken } from '../common/token.service';
import { IUser } from '../user/user.model';
import { userRepository } from '../user/user.repository';
import { authMeMapper } from './user.mapper';

class AuthService {
  getProfile(userId: number): Promise<IAuthMeDto> {
    return userRepository.get(userId)
      .then(model => authMeMapper.modelToDto(model))
  }
}

export const authService = new AuthService();
