import { NextFunction, Request, Response, Router } from 'express';
import { AuthenticatedRequest, authenticationMiddleware } from '../common/authentication.middleware';
import { userController } from './user.controller';

export const profileRouter = Router();

profileRouter.get('/:id',
  (req: Request, res: Response, next: NextFunction) => userController.look_profile(req, res, next));
