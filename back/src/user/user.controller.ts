import { NextFunction, Request, Response } from 'express';
import { AuthenticatedRequest } from '../common/authentication.middleware';
import { authService } from './user.service';

class UserController {

  look_profile(req: Request, res: Response, next: NextFunction): void {
    const credentials = parseInt(req.params.id, 10);
    authService.getProfile(credentials)
      .then(dto => res.json(dto))
      .catch(next);
  }
}

export const userController = new UserController();
