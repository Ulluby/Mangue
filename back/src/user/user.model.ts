import { DataTypes, Model } from 'sequelize';
import { IModel } from '../common/abstract.model';
import { sequelize } from '../common/database.config';

export enum UserRole {
  member = 'MEMBER',
  admin = 'ADMIN'
}

export interface IUser extends IModel {
  password: string;
  role?: UserRole;
  pseudo: string
}

const attributes = {
  password: {
    type: DataTypes.STRING,
    allowNull: false
  },
  role: {
    type: DataTypes.STRING,
    allowNull: true
  },
  pseudo: {
    type: DataTypes.STRING,
    allowNull: false
  }
}

const options = {
  sequelize,
  modelName: 'user'
};

export class UserModel extends Model implements IUser {
  id: number;
  password: string;
  role: UserRole;
  pseudo: string;
}
UserModel.init(attributes, options);
