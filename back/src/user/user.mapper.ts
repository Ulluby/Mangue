import { IAuthMeDto } from '../auth/auth.model';
import { AbstractMapper } from '../common/abstract.mapper';
import { IUser } from '../user/user.model';

class AuthMeMapper extends AbstractMapper<IUser, IAuthMeDto> {
  dtoToModel(dto: IAuthMeDto): IUser {
    return undefined;
  }

  modelToDto(model: any): IAuthMeDto {
    return {
      id: model.id,
      password: model.password,
      role: model.role,
      pseudo: model.pseudo,
    };
  }
}

export const authMeMapper = new AuthMeMapper();
