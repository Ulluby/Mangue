import {IUser} from '../user/user.model';
import {userRepository} from '../user/user.repository';

class Admin_userService {
    getUsers(): Promise<any> {
        return userRepository.findAll()
    }

    getUser(id: number): Promise<any> {
        return userRepository.get(id)
    }

    add(data: IUser): Promise<any> {
        return userRepository.create(data)
    }

    delete(id: number): Promise<any> {
        return userRepository.remove(id)
    }
}

export const admin_userService = new Admin_userService();
