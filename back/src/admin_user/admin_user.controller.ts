import { NextFunction, Request, Response } from 'express';
import { AuthenticatedRequest } from '../common/authentication.middleware';
import { admin_userService } from './admin_user.service';

class Admin_userController {
  getUsers(req: Request, res: Response, next: NextFunction): void {
    admin_userService.getUsers()
      .then(dto => res.json(dto))
      .catch(next);
  }
  getUser(req: Request, res: Response, next: NextFunction): void {
    const id = parseInt(req.params.id, 10);
    admin_userService.getUser(id)
        .then(dto => res.json(dto))
        .catch(next);
  }
  add(req: Request, res: Response, next: NextFunction): void {
    const data = req.body;
    console.log(data)
    admin_userService.add(data)
        .then(dto => res.json(dto))
        .catch(next);
  }
  delete(req: Request, res: Response, next: NextFunction): void {
    const id = parseInt(req.params.id, 10);
    admin_userService.delete(id)
        .then(dto => res.json(dto))
        .catch(next);
  }
}

export const admin_userController = new Admin_userController();
