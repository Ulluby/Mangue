import { IDto } from '../common/abstract.model';
import { UserRole } from '../user/user.model';

export interface IAuthCredentials {
  login: string;
  pwd: string;
}

export interface IAuthToken {
  token: string;
}

export interface IAuthMeDto extends IDto {
  pseudo: string;
  password: string;
  role: UserRole;
}
