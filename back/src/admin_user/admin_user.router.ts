import { NextFunction, Request, Response, Router } from 'express';
import { AuthenticatedRequest, authenticationMiddleware } from '../common/authentication.middleware';
import { admin_userController } from './admin_user.controller';

export const admin_userRouter = Router();

admin_userRouter.get('/users',
    (req: Request, res: Response, next: NextFunction) => admin_userController.getUsers(req, res, next));
admin_userRouter.get('/user/:id',
    (req: Request, res: Response, next: NextFunction) => admin_userController.getUser(req, res, next));
admin_userRouter.post('/user/add',
  (req: Request, res: Response, next: NextFunction) => admin_userController.add(req, res, next));
admin_userRouter.delete('/user/:id',
    (req: Request, res: Response, next: NextFunction) => admin_userController.delete(req, res, next));
