import { sign, verify } from 'jsonwebtoken';
import { UserRole } from '../user/user.model';

const secretKey = 'abcdef';

export interface ITokenPayload {
  id: number;
  pseudo: string;
  role: UserRole;
}

export const generateToken = (user, tokenPayload: ITokenPayload): Promise<any> => {
  return new Promise((resolve, reject) => {
    sign(tokenPayload, secretKey, (error, token) => {
      if (error) {
        reject(error);
      } else {
        resolve({user, token});
      }
    });
  });
}

export const checkToken = (token: string): Promise<ITokenPayload> => {
  return new Promise((resolve, reject) => {
    verify(token, secretKey, (error, payload) => {
      if (error) {
        reject(error);
      } else {
        resolve(payload as ITokenPayload);
      }
    });
  });
}
