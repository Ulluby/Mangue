import { Sequelize } from 'sequelize';

export const sequelize = new Sequelize('theodoum_mangue', 'theodoum_mangue', '3N7azYd3D', {
  host: 'mysql-theodoum.alwaysdata.net',
  dialect: 'mysql'
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
    return sequelize.sync();
  })
  .then(() => console.log('Database synchronized'))
  .catch(err => console.error('Database error:', err));
