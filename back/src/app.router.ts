import express from 'express';
import { authRouter } from './auth/auth.router';
import { profileRouter } from './user/user.router';
import { mangaRouter } from './manga/manga.router';
import { searchRouter } from './search/search.router';
import { admin_userRouter } from './admin_user/admin_user.router';
import { routeNotFoundMiddleware } from './common/route-not-found.middleware';

export const router = express.Router();
router.use('/admin', admin_userRouter);
router.use('/auth', authRouter);
router.use('/profile', profileRouter);
router.use('/manga', mangaRouter);
router.use('/search', searchRouter);
router.use(routeNotFoundMiddleware);
