const axios = require('axios');

class JikanService {
    axiosInstance = axios;

    apiUrl = 'https://api.jikan.moe/v3'

    getManga(id: number) {
        return this.axiosInstance.get(this.apiUrl + '/manga/' + id).then(response => {
            // handle success
            console.log(response);
            return response
        })
            .catch(function (error) {
                // handle error
                console.log(error);
            });
    }

    searchMangas(searchString: string) {
        return this.axiosInstance.get(this.apiUrl + '/search/manga?limit=4&genre_exclude=true&genre=12&q=' + searchString).then(response => {
            // handle success
            return response
        })
            .catch(function (error) {
                // handle error
                console.log(error);
            });
    }
}

export const jikanService = new JikanService();