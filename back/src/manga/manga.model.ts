import {DataTypes, Model} from 'sequelize';
import {IModel} from '../common/abstract.model';
import {sequelize} from '../common/database.config';

export interface IManga extends IModel {
    mal_id: number;
    title: string;
    title_english: string;
    synopsis: string;
    status: string;
    volumes: string;
    chapters: string;
    url: string;
    genres?: string;
}

const attributes = {
    mal_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    title: {
        type: DataTypes.STRING,
        allowNull: true
    },
    title_english: {
        type: DataTypes.STRING,
        allowNull: true
    },
    synopsis: {
        type: DataTypes.STRING,
        allowNull: true
    },
    status: {
        type: DataTypes.BOOLEAN,
        allowNull: true
    },
    volumes: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    chapters: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    url: {
        type: DataTypes.STRING,
        allowNull: true
    },
    genres: {
        type: DataTypes.TEXT,
        allowNull: true
    },
}

const options = {
    sequelize,
    modelName: 'manga'
};

export class MangaModel extends Model implements IManga {
    mal_id: number;
    title: string;
    title_english: string;
    synopsis: string;
    status: string;
    volumes: string;
    chapters: string;
    url: string;
    genres?: string;
}

MangaModel.init(attributes, options);
