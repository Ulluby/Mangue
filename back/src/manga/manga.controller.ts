import {NextFunction, Request, Response} from 'express';
import {mangaService} from './manga.service';
import {jikanService} from '../jikan/jikan.service';
import {IManga} from "./manga.model";

class MangaController {

    get(req: Request, res: Response, next: NextFunction): void {
        mangaService.getManga(parseInt(req.params.id, 10))
            .then(dto => res.json(dto))
            .catch(next);
    }

    getMangas(req: Request, res: Response, next: NextFunction): void {
        mangaService.getMangas()
            .then(dto => res.json(dto))
            .catch(next);
    }

    getRandom(req: Request, res: Response, next: NextFunction): void {
        mangaService.getRandomMangas()
            .then(dto => res.json(dto))
            .catch(next);
    }

    search(req: Request, res: Response, next: NextFunction): void {
        jikanService.searchMangas(req.params.searchValue)
            .then(data => {
                const computedData: IManga[] = data.data.results.map(item => {
                    return {
                        mal_id: item.mal_id,
                        title: item.title,
                        title_english: item.title_english,
                        synopsis: item.synopsis,
                        status: item.publishing,
                        volumes: item.volumes,
                        chapters: item.chapters,
                        url: item.image_url,
                    }
                })
                return res.json(computedData);
            })
    }

    delete(req: Request, res: Response, next: NextFunction): void {
        const id = parseInt(req.params.id, 10);
        mangaService.delete(id)
            .then(dto => res.json(dto))
            .catch(next);
    }

    add(req: Request, res: Response, next: NextFunction): void {
        const data = req.body;
        mangaService.getMangaByMalId(data.mal_id)
            .then(existing => {
                console.log(existing)
                if (existing) res.json();
                else {
                    //retrieve its genres
                    jikanService.getManga(data.mal_id).then(response => {
                            const formatedMangas: IManga = {
                                mal_id: response.data.mal_id,
                                title: response.data.title,
                                title_english: response.data.title_english,
                                synopsis: response.data.synopsis,
                                status: response.data.publishing,
                                volumes: response.data.volumes,
                                chapters: response.data.chapters,
                                url: response.data.image_url,
                                genres: JSON.stringify(response.data.genres),
                            }
                            mangaService.add(formatedMangas)
                                .then(dto => res.json(dto))
                                .catch(next);
                        }
                    ).catch(next);
                }
            })
            .catch(next);
    }
}

export const mangaController = new MangaController();
