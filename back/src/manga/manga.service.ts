import {ErrorType} from '../common/error/error.model';
import {mangaRepository} from './manga.repository';
import {mangaMapper} from './manga.mapper';

class MangaService {
    getManga(mangaId: number): Promise<any> {
        return mangaRepository.get(mangaId);
    }

    getMangas(): Promise<any> {
        return mangaRepository.findAll()
    }

    getMangaByMalId(malId: number): Promise<any> {
        return mangaRepository.getByMalId(malId);
    }

    getRandomMangas(): Promise<any> {
        return mangaRepository.findAll();
    }

    add(data: any): Promise<any> {
        return mangaRepository.create(data)
    }

    delete(id: number): Promise<any> {
        return mangaRepository.remove(id);
    }
}

export const mangaService = new MangaService();
