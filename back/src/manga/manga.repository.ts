import {response} from 'express';
import {PrimaryKeyError} from '../common/error/repository-error.model';
import {IManga, MangaModel} from './manga.model';
import {sequelize} from '../common/database.config';
import {ResolveOptions} from 'node:dns';

class MangaRepository<IManga> {
    protected modelClass = MangaModel;


    findAll(): Promise<IManga> {
        return this.modelClass.findAll() as unknown as Promise<IManga>;
    }

    get(id: number): Promise<IManga> {
        return this.modelClass.findByPk(id, {rejectOnEmpty: false}) as unknown as Promise<IManga>;
    }

    getByMalId(id: number): Promise<IManga> {
        return this.modelClass.findOne({where: {mal_id: id}}) as unknown as Promise<IManga>;
    }

    create(model: IManga): Promise<IManga> {
        return this.modelClass.create(model) as unknown as Promise<IManga>;
    }

    update(id: number, model: IManga): Promise<IManga> {
        return this.modelClass.update(model, {where: {id}})
            .then(([affectedRowsCount]) => {
                if (affectedRowsCount) {
                    return model;
                } else {
                    throw new PrimaryKeyError();
                }
            });
    }

    remove(id: number): Promise<void> {
        return this.modelClass.destroy({where: {id}})
            .then((affectedRowsCount) => {
                if (!affectedRowsCount) {
                    throw new PrimaryKeyError();
                }
            });
    }
}

export const mangaRepository = new MangaRepository();
