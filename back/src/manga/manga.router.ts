import {NextFunction, Request, Response, Router} from 'express';
import {AuthenticatedRequest, authenticationMiddleware} from '../common/authentication.middleware';
import {mangaController} from './manga.controller';

export const mangaRouter = Router();

mangaRouter.get('/search/:searchValue',
    (req: Request, res: Response, next: NextFunction) => mangaController.search(req, res, next));

mangaRouter.get('/all',
    (req: Request, res: Response, next: NextFunction) => mangaController.getMangas(req, res, next));

mangaRouter.get('/random',
    (req: Request, res: Response, next: NextFunction) => mangaController.getRandom(req, res, next));

mangaRouter.get('/:id',
    (req: Request, res: Response, next: NextFunction) => mangaController.get(req, res, next));

mangaRouter.post('/add',
    (req: Request, res: Response, next: NextFunction) => mangaController.add(req, res, next));

mangaRouter.delete('/delete/:id',
    (req: Request, res: Response, next: NextFunction) => mangaController.delete(req, res, next));

