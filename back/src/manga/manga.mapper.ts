import { IAuthMeDto } from '../auth/auth.model';
import { AbstractMapper } from '../common/abstract.mapper';
import { IManga } from './manga.model';

class MangaMapper extends AbstractMapper<IManga, IAuthMeDto> {
  dtoToModel(dto: IAuthMeDto): IManga {
    return undefined;
  }

  modelToDto(model: any): IAuthMeDto {
    return {
      id: model.id,
      password: model.password,
      role: model.role,
      pseudo: model.pseudo,
    };
  }
}

export const mangaMapper = new MangaMapper();
