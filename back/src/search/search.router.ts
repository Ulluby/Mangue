import {NextFunction, Request, Response, Router} from 'express';
import {mangaController} from '../manga/manga.controller';

export const searchRouter = Router();

searchRouter.get('/manga/:searchValue',
    (req: Request, res: Response, next: NextFunction) => mangaController.search(req, res, next));
