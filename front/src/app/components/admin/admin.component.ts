import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {ProfileService} from "../../services/profile.service";
import {MangaService} from "../../services/manga.service";
import {IUser} from "../../interfaces/IUser";
import {IManga} from "../../interfaces/IManga";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent {

  users: IUser[];
  mangas: IManga[];

  constructor(private router: Router, private profileService: ProfileService, private mangaService: MangaService) {
    this.getUsers();
    this.getMangas();

  }

  deleteUser(user: IUser) {
    this.profileService.delete(user.id).subscribe(res => {
      console.log(res)
      this.getUsers();
    });
  }

  deleteManga(manga: IManga) {
    this.mangaService.delete(manga.id).subscribe(res => {
      console.log(res)
      this.getMangas();
    });
  }

  getMangas() {
    this.mangaService.getMangas().subscribe(res => {
      this.mangas = res;
    })
  }

  getUsers() {
    this.profileService.getUsers().subscribe(res => {
      this.users = res;
    })
  }
}
