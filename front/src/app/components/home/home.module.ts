import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from "./home.component";
import {CardSectionModule} from "../card-section/card-section.module";

@NgModule({
  declarations: [HomeComponent],
  exports: [HomeComponent],
  imports: [
    CommonModule,
    CardSectionModule,
  ]
})
export class HomeModule {
}
