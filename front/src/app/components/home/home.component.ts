import {Component, OnInit} from '@angular/core';
import {IManga} from "../../interfaces/IManga";
import {MangaService} from "../../services/manga.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  popularMangaTitle: string = "Popular"
  randomMangaTitle: string = "Random"

  popularMangas: IManga[];
  reducedPopularMangas: IManga[];
  randomMangas: IManga[];
  reducedRandomMangas: IManga[];

  constructor(private mangaService: MangaService) {
    this.getPopularMangas();
    this.getRandomMangas();
  }

  getPopularMangas(): void { //add subscription later
    this.popularMangas = this.mangaService.getPopularMangas();
    // this.mangaService.getPopularMangas().subscribe(mangas => {
    //   this.popularMangas = mangas;
    // });
  }

  getRandomMangas(): void { //add subscription later
    this.mangaService.getRandomMangas().subscribe(mangas => {
      console.log(mangas)
      this.randomMangas = mangas;
      this.reducedRandomMangas = mangas.slice(0, 4);
    });
  }
}
