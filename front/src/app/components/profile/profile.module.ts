import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProfileComponent} from "./profile.component";

import {CardSectionModule} from "../card-section/card-section.module";

@NgModule({
  declarations: [ProfileComponent],
  exports: [ProfileComponent],
  imports: [
    CommonModule,
    CardSectionModule
  ]
})
export class ProfileModule {
}
