import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ProfileService} from "../../services/profile.service";
import {IUser} from "../../interfaces/IUser";
import {IManga} from "../../interfaces/IManga";
import {MangaService} from "../../services/manga.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent {
  user;
  favMangaTitle: string = "Fav"
  readLaterMangaTitle: string = "Read later"

  favMangas: any[]; //todo
  readLaterMangas: any[];


  constructor(private route: ActivatedRoute, private profileService: ProfileService, private mangaService: MangaService) {
    this.route.params
      .subscribe(params => {
          const id = parseInt(params.id, 10);
          let myProfile = profileService.getMyProfile()
          if (id === myProfile.id) {
            this.user = myProfile;
          } else {
            this.user = profileService.get(id);
          }

          this.getUserFavMangas(params.id);
          this.getUserReadLaterMangas(params.id);
        }
      );
  }


  getUserFavMangas(id): void { //add subscription later
    this.favMangas = this.mangaService.getUserFavMangas(id);
    // this.mangaService.getfavMangas().subscribe(mangas => {
    //   this.favMangas = mangas;
    // });
  }

  getUserReadLaterMangas(id): void { //add subscription later
    this.readLaterMangas = this.mangaService.getUserReadLaterMangas(id);
    // this.mangaService.getreadLaterMangas().subscribe(mangas => {
    //   this.readLaterMangas = mangas;
    // });
  }
}
