import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';


import {NavbarComponent} from './navbar.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {MatSidenav, MatSidenavModule} from "@angular/material/sidenav";
import {MatListModule} from '@angular/material/list';
import {MatTooltipModule} from '@angular/material/tooltip';
import {RouterModule} from '@angular/router';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {LoginModule} from "../login/login.module";
import {SignupModule} from "../signup/signup.module";
import {ProfileModule} from "../profile/profile.module";
import {HomeModule} from "../home/home.module";
import {MangaPageModule} from "../manga-page/manga-page.module";
import {MangaListModule} from "../manga-list/manga-list.module";
import {HttpClientModule} from "@angular/common/http";
import {AddMangaModule} from "../add-manga/add-manga.module";
import {AdminModule} from "../admin/admin.module";
import {AddUserModule} from "../add-user/add-user.module";


@NgModule({
  declarations: [
    NavbarComponent
  ],
  exports: [
    NavbarComponent
  ],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatSidenavModule,
    MatListModule,
    RouterModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,

    HttpClientModule,

    LoginModule,
    SignupModule,
    ProfileModule,
    HomeModule,
    MangaPageModule,
    MangaListModule,
    AdminModule,
    AddMangaModule,
    AddUserModule
  ]
})
export class NavbarModule {
}
