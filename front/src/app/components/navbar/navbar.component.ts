import {Component, OnInit} from '@angular/core';
import {ProfileService} from "../../services/profile.service";
import {NavigationEnd, Router, RouterEvent} from "@angular/router";
import {filter} from 'rxjs/operators'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  user;

  constructor(private profileService: ProfileService, private router: Router) {
    this.user = this.profileService.getMyProfile();
  }

  logout(): void {
    this.user = null;
    this.profileService.logout().then(() => {
      this.router.navigate(['']);
    });
  }

  ngOnInit(): void {
    this.router.events.pipe(
      filter((event: RouterEvent) => event instanceof NavigationEnd)
    ).subscribe(() => {
      this.user = this.profileService.getMyProfile();
    });
  }
}
