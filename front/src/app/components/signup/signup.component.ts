import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {IUser} from "../../interfaces/IUser";
import {AuthService} from "../../services/auth.service";
import {ProfileService} from "../../services/profile.service";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {
  formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
  ) {
    this.formGroup = this.formBuilder.group({
      pseudo: [undefined, [Validators.required, Validators.minLength(3)]],
      password: [undefined, Validators.required],
    });
    this.reset();
  }

  reset(): void {
    this.formGroup.reset({});
  }

  save(): void {
    if (this.formGroup.valid) {
      const formValue = this.formGroup.value;
      const user: IUser = {
        pseudo: formValue.pseudo,
        password: formValue.password,
      };
      this.authService.save(user).subscribe(res => {
        if (res) {
          this.router.navigate(['/login'])
        }
      });
    }
  }
}
