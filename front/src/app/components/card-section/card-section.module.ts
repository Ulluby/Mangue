import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CardSectionComponent} from "./card-section.component";
import {MatListModule} from "@angular/material/list";
import {MatButtonModule} from "@angular/material/button";
import {MangaCardModule} from "../manga-card/manga-card.module";


@NgModule({
  declarations: [CardSectionComponent],
  exports: [CardSectionComponent],
  imports: [
    CommonModule,
    MatListModule,
    MatButtonModule,

    MangaCardModule
  ]
})
export class CardSectionModule { }
