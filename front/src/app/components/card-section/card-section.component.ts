import {Component, Input, OnInit} from '@angular/core';
import {IManga} from "../../interfaces/IManga";

@Component({
  selector: 'app-card-section',
  templateUrl: './card-section.component.html',
  styleUrls: ['./card-section.component.css']
})
export class CardSectionComponent {

  @Input()
  title: string;
  @Input()
  data: IManga[];
  @Input()
  noLink: boolean = false;
  @Input()
  showAdd: boolean = false;
  @Input()
  hideAll: boolean = false;
  @Input()
  onclick: any = null;

  constructor() {
  }

}
