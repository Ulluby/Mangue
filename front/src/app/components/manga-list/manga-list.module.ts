import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MangaListComponent} from "./manga-list.component";


@NgModule({
  declarations: [MangaListComponent],
  exports: [MangaListComponent],
  imports: [
    CommonModule
  ]
})
export class MangaListModule {
}
