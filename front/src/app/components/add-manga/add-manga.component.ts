import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MangaService} from "../../services/manga.service";
import {Router} from "@angular/router";
import {IManga} from "../../interfaces/IManga";

@Component({
  selector: 'app-add-manga',
  templateUrl: './add-manga.component.html',
  styleUrls: ['./add-manga.component.css']
})
export class AddMangaComponent {
  searchResultsTitle: string = "Résultats";
  searchResults: IManga[];
  formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private mangaService: MangaService,
    private router: Router
  ) {
    this.formGroup = this.formBuilder.group({
      search: [undefined, [Validators.required, Validators.minLength(3)]],
    });
    this.reset();
  }

  reset(): void {
    this.formGroup.reset({});
  }

  search(): void {
    if (this.formGroup.valid) {
      const formValue = this.formGroup.value;
      this.mangaService.search(formValue.search).subscribe(res => {
        this.searchResults = res;
      });
    }
  }


  addManga = (result: IManga) => {
    this.mangaService.add(result).subscribe(res => {
      console.log(res)
    });
  }
}
