import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AddMangaComponent} from "./add-manga.component";
import {ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {CardSectionModule} from "../card-section/card-section.module";
import {MangaCardModule} from "../manga-card/manga-card.module";



@NgModule({
  declarations: [AddMangaComponent],
  exports: [AddMangaComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    CardSectionModule,
    MangaCardModule
  ]
})
export class AddMangaModule { }
