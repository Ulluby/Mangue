import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MangaPageComponent} from "./manga-page.component";
import {HttpClientModule} from "@angular/common/http";
import {MatChipsModule} from "@angular/material/chips";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";

@NgModule({
  declarations: [MangaPageComponent],
  exports: [MangaPageComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    MatChipsModule,
    MatIconModule,
    MatButtonModule
  ]
})
export class MangaPageModule {
}
