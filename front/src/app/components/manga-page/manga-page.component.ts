import {Component, OnInit} from '@angular/core';
import {MangaService} from "../../services/manga.service";
import {ActivatedRoute} from "@angular/router";
import {ProfileService} from "../../services/profile.service";
import {IManga} from "../../interfaces/IManga";
import {Location} from '@angular/common';

@Component({
  selector: 'app-manga-page',
  templateUrl: './manga-page.component.html',
  styleUrls: ['./manga-page.component.css']
})
export class MangaPageComponent {

  manga;
  user = this.profileService.getMyProfile();

  constructor(private profileService: ProfileService, private mangaService: MangaService, private route: ActivatedRoute, private location: Location) {
    this.getManga(this.route.snapshot.params.id)
  }

  getManga(id) {
    this.mangaService.getManga(id).subscribe(manga => this.manga = {...manga, genres: JSON.parse(manga.genres)});
  }

  delete(manga: IManga) {
    this.mangaService.delete(manga.id).subscribe(res => {
      this.location.back();
    });
  }
}
