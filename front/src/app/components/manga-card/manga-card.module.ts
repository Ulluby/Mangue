import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MangaCardComponent} from "./manga-card.component";
import {RouterModule} from "@angular/router";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";


@NgModule({
  declarations: [MangaCardComponent],
  exports: [MangaCardComponent],
    imports: [
        CommonModule,
        RouterModule,
        MatButtonModule,
        MatIconModule
    ]
})
export class MangaCardModule {
}
