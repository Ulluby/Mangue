import {Component, Input, OnInit} from '@angular/core';
import {IManga} from "../../../../../back/src/manga/manga.model";

@Component({
  selector: 'app-manga-card',
  templateUrl: './manga-card.component.html',
  styleUrls: ['./manga-card.component.css']
})
export class MangaCardComponent implements OnInit {

  @Input() item: IManga;
  @Input() noLink = false;
  @Input() showAdd = false;
  @Input() addManga = null;
  @Input() onclick = null;

  constructor() {
  }

  ngOnInit(): void {
  }

  saveManga(item: any) {
    this.onclick(item)
  }
}
