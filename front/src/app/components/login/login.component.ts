import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Observable} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {IUserAuth} from "../../interfaces/IUser";
import {AuthService} from "../../services/auth.service";
import {ProfileService} from "../../services/profile.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private profileService: ProfileService,
    private router: Router
  ) {
    this.formGroup = this.formBuilder.group({
      pseudo: [undefined, [Validators.required, Validators.minLength(3)]],
      password: [undefined, Validators.required],
    });
    this.reset();
  }

  reset(): void {
    this.formGroup.reset({});
  }

  save(): void {
    if (this.formGroup.valid) {
      const formValue = this.formGroup.value;
      const user: IUserAuth = {
        login: formValue.pseudo,
        pwd: formValue.password,
      };
      this.authService.login(user).subscribe(res => {
        if (res) {
          this.profileService.setUser(res);
          this.profileService.setToken(res);
          this.router.navigate(['']);
        }
      });
    }
  }
}
