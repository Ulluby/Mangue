import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth.service";
import {ProfileService} from "../../services/profile.service";
import {Router} from "@angular/router";
import {IUser, IUserAuth} from "../../interfaces/IUser";

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent {

  formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private profileService: ProfileService,
    private router: Router
  ) {
    this.formGroup = this.formBuilder.group({
      pseudo: [undefined, [Validators.required, Validators.minLength(3)]],
      password: [undefined, [Validators.required, Validators.minLength(3)]],
      role: [undefined, Validators.required],
    });
    this.reset();
  }

  reset(): void {
    this.formGroup.reset({});
  }

  save(): void {
    if (this.formGroup.valid) {
      const formValue = this.formGroup.value;
      const user: IUser = {
        pseudo: formValue.pseudo,
        password: formValue.password,
        role: formValue.role,
      };
      this.profileService.add(user).subscribe(res => {
        console.log(res)
        this.router.navigate(['admin']);
      })
    }
  }

}
