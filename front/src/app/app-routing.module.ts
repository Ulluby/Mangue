import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from "./components/login/login.component";
import {SignupComponent} from "./components/signup/signup.component";
import {ProfileComponent} from "./components/profile/profile.component";
import {HomeComponent} from "./components/home/home.component";
import {MangaPageComponent} from "./components/manga-page/manga-page.component";
import {AdminComponent} from "./components/admin/admin.component";
import {AddMangaComponent} from "./components/add-manga/add-manga.component";
import {AddUserComponent} from "./components/add-user/add-user.component";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: 'profile/:id',
    component: ProfileComponent
  },
  {
    path: 'manga/:id',
    component: MangaPageComponent
  },
  {
    path: 'admin',
    component: AdminComponent
  },
  {
    path: 'admin/manga/add',
    component: AddMangaComponent
  },
  {
    path: 'admin/user/add',
    component: AddUserComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
