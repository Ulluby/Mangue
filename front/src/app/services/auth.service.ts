import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {IUser, IUserAuth} from "../interfaces/IUser";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {
  }

  save(user: IUser) {
    return this.http.post(environment.apiUrl + "/auth/inscription", user, {})
  }

  login(user: IUserAuth) {
    return this.http.post(environment.apiUrl + "/auth/login", user, {})
  }
}
