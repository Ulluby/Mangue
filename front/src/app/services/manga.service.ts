import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {IManga} from "../interfaces/IManga";

@Injectable({
  providedIn: 'root'
})
export class MangaService {

  tempEntries = [
    {
      "mal_id": 556,
      "title": "Trinity Blood",
      "title_english": "Trinity Blood",
      "status": "0",
      "url": "https:\/\/cdn.myanimelist.net\/images\/manga\/1\/164661.jpg",
      "volumes": 21,
      "chapters": 89,
      "synopsis": "In the distant future, Armageddon has caused the appearance of a second \"Vampire's\" moon. This sparks the beginning of a war between humans and \"Methuselah,\" the vampires that feed on their blood. Esther Blanchett is an orphan who was raised in the Church of St. Matthias in the city of Istvan. One day, she meets Abel Nightroad, a priest sent to subdue any threats to the Vatican, an organization allied with humanity. Beneath Abel's meek facade hides his real identity as \"The Crusnik\": vampire who preys on the blood of his own kind and the Vatican's greatest weapon. Esther's peaceful times are cut short when the church is bombed and she is betrayed by her only friend Dietrich. With nowhere else to go, she teams up with Abel and Tres Iqus, a machine disguised as a human, to travel to Rome and look for answers behind the devastation of her home. However, their journey will not be easy, as they will have to battle various obstacles as well as discover the secrets of Abel's past. [Written by MAL Rewrite]",
      "genres": [{
        "mal_id": 1,
        "type": "manga",
        "name": "Action",
        "url": "https:\/\/myanimelist.net\/manga\/genre\/1\/Action"
      }, {
        "mal_id": 8,
        "type": "manga",
        "name": "Drama",
        "url": "https:\/\/myanimelist.net\/manga\/genre\/8\/Drama"
      }, {
        "mal_id": 10,
        "type": "manga",
        "name": "Fantasy",
        "url": "https:\/\/myanimelist.net\/manga\/genre\/10\/Fantasy"
      }, {
        "mal_id": 14,
        "type": "manga",
        "name": "Horror",
        "url": "https:\/\/myanimelist.net\/manga\/genre\/14\/Horror"
      }, {
        "mal_id": 24,
        "type": "manga",
        "name": "Sci-Fi",
        "url": "https:\/\/myanimelist.net\/manga\/genre\/24\/Sci-Fi"
      }, {
        "mal_id": 25,
        "type": "manga",
        "name": "Shoujo",
        "url": "https:\/\/myanimelist.net\/manga\/genre\/25\/Shoujo"
      }, {
        "mal_id": 32,
        "type": "manga",
        "name": "Vampire",
        "url": "https:\/\/myanimelist.net\/manga\/genre\/32\/Vampire"
      }],
      "authors": [{
        "mal_id": 2494,
        "type": "people",
        "name": "Sunao, Yoshida",
        "url": "https:\/\/myanimelist.net\/people\/2494\/Yoshida_Sunao"
      }, {
        "mal_id": 2496,
        "type": "people",
        "name": "Kyuujou, Kiyo",
        "url": "https:\/\/myanimelist.net\/people\/2496\/Kiyo_Kyuujou"
      }],
    }
  ]

  constructor(private http: HttpClient) {
  }

  getPopularMangas(): any { // todo avec le service api backend + mapper
    return this.tempEntries;
  }

  getRandomMangas(): Observable<any> {
    return this.http.get(environment.apiUrl + '/manga/random');
  }

  getMangas(): Observable<any> {
    return this.http.get(environment.apiUrl + '/manga/all');
  }


  getUserFavMangas(id: string | number) {
    return this.tempEntries;
  }

  getUserReadLaterMangas(id: string | number) {
    return this.tempEntries;
  }

  getManga(id: string | number): Observable<any> {
    return this.http.get(environment.apiUrl + '/manga/' + id);
  }

  search(searchValue: string): Observable<any> {
    return this.http.get(environment.apiUrl + '/search/manga/' + searchValue);
  }

  add(manga: IManga): Observable<any> {
    return this.http.post(environment.apiUrl + '/manga/add/', manga);
  }

  delete(id: number): Observable<any> {
    console.log(id)
    return this.http.delete(environment.apiUrl + '/manga/delete/' + id);
  }
}
