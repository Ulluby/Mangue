import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {IUser} from "../interfaces/IUser";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private http: HttpClient) {
  }

  getMyProfile() {
    let user = localStorage.getItem('user')
    if (user) {
      return JSON.parse(user)
    }
    return null;
  }

  setUser(res) {
    localStorage.setItem("user", JSON.stringify(res.user));
  }

  setToken(res) {
    localStorage.setItem("token", res.token);
  }

  logout(): Promise<any> {
    localStorage.removeItem("token");
    localStorage.removeItem("user");

    return Promise.resolve()
  }

  getUsers(): Observable<any> {
    return this.http.get(environment.apiUrl + '/admin/users/');
  }

  get(id: number): Observable<any> {
    return this.http.get(environment.apiUrl + '/admin/user/' + id);
  }

  add(user: IUser): Observable<any> {
    return this.http.post(environment.apiUrl + '/admin/user/add', user);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(environment.apiUrl + '/admin/user/' + id);
  }
}
