export interface IManga {
  id?: number;
  mal_id?: number;
  title?: string;
  title_english: string;
  synopsis: string;
  status: string;
  volumes: string;
  chapters: string;
  url: string;
  genres?: string;
}
