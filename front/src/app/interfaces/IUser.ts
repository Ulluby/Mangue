export interface IUser {
  id?: number,
  pseudo: string,
  password?: string,
  role?: string,
}
export interface IUserAuth {
  login: string,
  pwd: string,
}
